# mx-wichita

[MustangX](http://www.mustangx.org), codenamed: **[Wichita](https://gitlab.com/MustangX/mx-wichita)**, is the first version to be built from scratch. **[Wichita](https://gitlab.com/MustangX/mx-wichita)** may stay as the development name, simular to [Debian's](http://debian.org) - *Sid*

## What features are planned?

#### Planned **CORE** FEATURES

* Integreted _PAGES_ with _Catagories_
  * Create pages that do not change often. Sucha as:
    * Legal Pages
    * Tutorials
    * About Us
  * Often called *Static Pages*.

* EU Cookie Law Compliance
  * No theme dependance

* Captcha and/or reCaptcha

* Contact Page

* Update/Upgrade Method
  * Something like WP has (drupal is ok too)

* Bootstrap

* JQuery



#### Planned Requirements

* PHP 7+
* MySQLi (pdo-mysqli prefered)
* Webserver
   * Apache2 or nGinX (maybe others)
   * PHP 7
   * MySQL





## Documention

Visit the [WIKI](https://gitlab.com/MustangX/mx-wichita/wikis/home)


## License

[](https://gitlab.com/MustangX/mx-wichita/blob/master/LICENSE)


### Changelog info

##### Guiding Principles

  * Changelogs are for humans, not machines.
  * There should be an entry for every single version.
  * The same types of changes should be grouped.
  * Versions and sections should be linkable.
  * The latest version comes first.
  * The release date of each versions is displayed. (Format= Year-Month-Day)
  * This project follows the [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

##### Types of changes

  * **Added** for new features.
  * **Changed** for changes in existing functionality.
  * **Deprecated** for soon-to-be removed features.
  * **Removed** for now removed features.
  * **Fixed** for any bug fixes.
  * **Security** in case of vulnerabilities.
